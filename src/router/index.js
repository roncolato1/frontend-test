import { createRouter, createWebHistory } from 'vue-router'
import ListPeople from '../views/ListPeople.vue'
import DetailPeople from '../views/DetailPeople.vue'

import AppLayout from '../layouts/AppLayout.vue';

// const BASE_API_URL = 'https://dummyjson.com/'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'listPeople',
            component: ListPeople,
            meta: {
                layout: AppLayout
            }
        },
        {
            path: '/users/:id',
            name: 'detail_people',
            component: DetailPeople,
            meta: {
                layout: AppLayout
            }
        },
    ]
})

export default router
