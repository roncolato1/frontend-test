export default {
    install: (app, options) => {
        app.config.globalProperties.$isMobile = () => {
            return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
        }
        app.provide('detectMobile', options)
      }
  }
