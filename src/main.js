import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import detectMobile from './plugins/detectMobile'

const app = createApp(App)

app.provide( 'BASE_API_URL', 'https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/')
app.provide( 'API_TOKEN', 'keyEC9F0usYdVoWzg')

app.use(router)
app.use(detectMobile)

app.mount('#app')
