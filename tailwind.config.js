module.exports = {
    darkMode: false, // or 'media' or 'class'
    content: [
        "./index.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {
            fontFamily: {
                sans: ['Work Sans', 'sans'],
            },
            colors:{
                sand: {
                    '200': '#E0DBD0',
                    '500': '#F6F4EF'
                },
                gray: {
                    '500': '#828282',
                    '900': '#333333'
                },
                green: {
                    '400': '#8DCC7B'
                }
            },
            fontSize: {
                'sm': ['13px', {
                    lineHeight: '15px',
                    letterSpacing: '-1px'
                }],
                'base': ['16px', {
                    lineHeight: '19px',
                    letterSpacing: '-1px'
                }],
                'xl': ['20px',{
                    lineHeight: '23px',
                    letterSpacing: '-1px'
                }],
                '2xl': ['34px',{
                    lineHeight: '39px',
                    letterSpacing: '-1px'
                }],
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/aspect-ratio'),
    ],
}
